Proxmox

1. [Install](docs/proxmox.md)
2. [Virtual machine management](docs/qm_commands.md)
    - [Automated template creation](create_vm_template.sh)
        - Don't install qemu agent this causes ip conflicts (machine id may not be unique
        - Don't user serial0 for the display, this causes connection issues. When you don't mention this option Proxmox uses default settings which work.
    - [Automated virtual machine creation](create_vm.sh)

3. [Cluster management](docs/cluster_management.md)
4. [API](docs/api.md)

## Creating virtual machine

If you want to create a semi automated way of creating virtual machines, you first need to create a [Proxmox template](create_vm_template.sh) and then use the [create_vm.sh](create_vm.sh) script to create a virtual machine by cloning the template created using the create_vm_template.sh script.

Ideally you can copy the scrips to the Proxmox nodes and then execute the script from there or if you have control server that has ssh access to Proxmox nodes you can execute the node remotely

- ```ssh user@pve-1 bash -s < create_vm_template.sh```, to create the template
- ```ssh user@pve-1 bash -s < create_cm.sh```, to create a server based on the template created from above.