# User management

## View all users

```bash
root@pve-1:~# pveum user list
┌────────────┬─────────┬────────────────────────┬────────┬────────┬───────────┬────────┬──────┬──────────┬────────────┬────────┐
│ userid     │ comment │ email                  │ enable │ expire │ firstname │ groups │ keys │ lastname │ realm-type │ tokens │
╞════════════╪═════════╪════════════════════════╪════════╪════════╪═══════════╪════════╪══════╪══════════╪════════════╪════════╡
│ cmills@pve │ My user │ cmmills@protonmail.com │ 1      │      0 │           │        │      │          │ pve        │        │
├────────────┼─────────┼────────────────────────┼────────┼────────┼───────────┼────────┼──────┼──────────┼────────────┼────────┤
│ root@pam   │         │ cmmills@protonmail.com │ 1      │      0 │           │        │      │          │ pam        │        │
└────────────┴─────────┴────────────────────────┴────────┴────────┴───────────┴────────┴──────┴──────────┴────────────┴────────┘
```

### Add a user

Use the following command line option to add user to the Proxmox system.
```pveum user add cmills@pve -email <email-address>  -comment "My user" -password <password>```
the above command will allow the create user to login to the system but will not have any permisions and will not be able to see anythin.

### Grant read only access

This permission allows a user to view only 
```pveum acl modify / -user cmills@pve -role PVEAuditor```
- if the user is already logged in this will take affect instantly and does not require the user to log in and out.

- to see all available roles:
```bash
root@pve-1:~# pveum role list
┌───────────────────┬───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ roleid            │ privs
╞═══════════════════╪═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
│ Administrator     │ Datastore.Allocate,Datastore.AllocateSpace,Datastore.AllocateTemplate,Datastore.Audit,Group.Allocate,Permissions.Modify,Pool.Allocate,Pool.Audit,Realm.Allocate,Realm.
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ NoAccess          │
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEAdmin          │ Datastore.Allocate,Datastore.AllocateSpace,Datastore.AllocateTemplate,Datastore.Audit,Group.Allocate,Permissions.Modify,Pool.Allocate,Pool.Audit,Realm.AllocateUser,SD
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEAuditor        │ Datastore.Audit,Pool.Audit,SDN.Audit,Sys.Audit,VM.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEDatastoreAdmin │ Datastore.Allocate,Datastore.AllocateSpace,Datastore.AllocateTemplate,Datastore.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEDatastoreUser  │ Datastore.AllocateSpace,Datastore.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEPoolAdmin      │ Pool.Allocate,Pool.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEPoolUser       │ Pool.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVESDNAdmin       │ SDN.Allocate,SDN.Audit
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVESysAdmin       │ Permissions.Modify,Sys.Audit,Sys.Console,Sys.Syslog
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVETemplateUser   │ VM.Audit,VM.Clone
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEUserAdmin      │ Group.Allocate,Realm.AllocateUser,User.Modify
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEVMAdmin        │ VM.Allocate,VM.Audit,VM.Backup,VM.Clone,VM.Config.CDROM,VM.Config.CPU,VM.Config.Cloudinit,VM.Config.Disk,VM.Config.HWType,VM.Config.Memory,VM.Config.Network,VM.Config
├───────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
│ PVEVMUser         │ VM.Audit,VM.Backup,VM.Config.CDROM,VM.Config.Cloudinit,VM.Console,VM.PowerMgmt
└───────────────────┴───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
```
