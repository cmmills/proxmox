# Proxmox

## PVE setup
- install cockpit, this will allow you to view hardware and system information from a web GUI interface outside of proxmox.
-- ```apt install -y cockpit```

## Virtual machine

### Setup
- CPU
-- Type: host (Better performance)
- Disk
-- VirtIO Block
-- Discard on

### After setup

- install qemu-guest-agent
-- ```apt install -y qemu-guest-agent```

### Create template

Clean unused packages
- apt clean -y
- apt autoremove -y

Delete ssh host keys
- rm /etc/ssh/ssh_host_*

Empty machine id file
- truncate -s 0 /etc/machine-id
- rm /var/lib/dbus/machine-id
- ln -s /etc/machine-id /var/lib/machine-id

- power off the virtual machine
- right click on the virtual machine and select 'convert to template`
- 

## Create virtual machine from template
### recreate ssh host key files

Follow these steps to regenerate OpenSSH Host Keys

- Delete old ssh host keys: rm /etc/ssh/ssh_host_*
- Reconfigure OpenSSH Server: dpkg-reconfigure openssh-server
- Update all ssh client(s) ~/.ssh/known_hosts files