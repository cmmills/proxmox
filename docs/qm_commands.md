# QEMU commands

## Virtual machines

### Delete a virtual machine
```qm destroy 103 --purge```

### Stop virtual machine

- stop virtual machine
```qm stop 105```

- force stop a virtual machine, when stop command fails
```bash
root@pve-2:~# qm stop 105 -timeout 0
VM quit/powerdown failed - terminating now with SIGTERM
VM still running - terminating now with SIGKILL
```
### Clone vm
```qm clone 9999 103 --name test-clone --full --storage ssd_zfs --description "test vm"```
- use qm help clone from the commmand line to get full options

### Resize virtual drive
```qm resize 103 scsi0 +20G```

### Migrate virtual machine

```qm migrate 105 pve-2 --online```

For the above command to work quickly and without any errors
- the CPU settings should be set that all host can support (default qemu4)
- Storage should be on a fast shared network storage, NFS, CEPH etc.
- The destination host should have the virtual machine defined memory and CPU.