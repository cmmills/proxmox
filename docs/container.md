# Container

## Create a container using the command line
``` pct create 120 qnap:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz --storage qnap --cpuunits 2 --memory 512 --net0 name=eth0,bridge=vmbr0,ip=dhcp,firewall=1 --password="$password" --hostname jellyfin --start 1 --onboot 1```

### View available storage on a node
```pvesm status```

output
```bash
root@pve-2:~# pvesm status
Name             Type     Status           Total            Used       Available        %
local             dir     active        98497780         3087580        90360652    3.13%
local-lvm     lvmthin     active       354791424               0       354791424    0.00%
qnap             nfs     active       780233184       305275840       474433056   39.13%
ssd_zfs       zfspool   disabled               0               0               0      N/A
```

### List containers

```bash
root@pve-2:~# pct list
VMID       Status     Lock         Name
120        running                 jellyfin
```

### Create storage for the container
pvesm = Proxmox VE Storage Manager

```bash
root@pve-2:~# pvesm alloc qnap 120 '' 40G
Formatting '/mnt/pve/qnap/images/120/vm-120-disk-0.raw', fmt=raw size=42949672960 preallocation=off
successfully created 'qnap:120/vm-120-disk-0.raw'
```

### Remove a container

```pct destroy 120 --purge```
- Note: you need to stop the container prior to destroying. ```pct stop 120```


### Unlock a container

```pct unlock 107```
The above command can be used when the container is locked when migrating, proxmox turns off or becomes non-responsive


