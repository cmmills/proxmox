#!/usr/bin/env bash
vm_name="$1"
comment="$2"
vmid="125"
storage="qnap"
template_id="9997"

if [[ $# -ne 2 ]]; then
    echo "Enter parameters: server name and comment"
else
    echo -n "Enter password:";read -s password
    qm clone $template_id $vmid --name $vm_name --full --storage $storage --description "$comment"
    #qm set $vmid --ciuser cmills --cipassword $password --sshkey id_rsa.pub --ipconfig0 ip=dhcp
    qm start $vmid
fi
