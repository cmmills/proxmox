#! /usr/bin/env bash
# https://pve.proxmox.com/wiki/Cloud-Init_Support

img_name="focal-server-cloudimg-amd64.img"
os_image="https://cloud-images.ubuntu.com/focal/current/$img_name"
template_name="ubuntu-2004-template"
proxmox_id="9997"
storage="qnap"
disk_size="20G"

# installing libguestfs-tools (virt commands) only required once, prior to first run
 apt update -y
 apt install libguestfs-tools -y

# remove existing image in case last execution did not complete successfully
if [ -f "$img_name" ]; then rm $img_name; fi

# Download Image
wget $os_image

# Add the qemu-agent to the image, Adding this causes ip conflicts in DHCP,machine_id may not be unique
# virt-customize -a $img_name --install qemu-guest-agent

# Create the virtual machine
 qm create $proxmox_id --name "$template_name" --memory 2048 --cores 2 --net0 virtio,bridge=vmbr0
 qm importdisk $proxmox_id $img_name $storage
 qm set $proxmox_id --scsihw virtio-scsi-pci --scsi0 $storage:$proxmox_id/vm-$proxmox_id-disk-0.raw
 qm set $proxmox_id --boot c --bootdisk scsi0

# Create cloudinit file and options
qm set $proxmox_id --ide2 $storage:cloudinit
qm set $proxmox_id --ciuser cmills --cipassword $password --sshkey id_rsa.pub --ipconfig0 ip=dhcp

# Resize the disk to 20G
 qm resize $proxmox_id scsi0 $disk_size

# Convert the virtual machine to a template
 qm template $proxmox_id
